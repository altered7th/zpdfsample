package sample;

import java.io.IOException;
import zpdf.ZPdf;

/**
 * 最小のサンプル
 */
public class SimpleSample extends ZPdf {
	public static void main( String[] args ) {
		new SimpleSample();
	}
	public SimpleSample() {
		try {
			createPdf();
		} catch ( IOException e ) {
			e.printStackTrace();
		}
	}
	private void createPdf() throws IOException {
		// c.pdf を A4横で作成する
		initPdf( "c.pdf", A4_LANDSCAPE );

		// 何か出力する必要がある。空の PDF はエラーになる。
		drawFullOutline();	// アウトラインを出力する
		showGrid();

		closePdf();	// ドキュメントをクローズ
		browse();	// デスクトップに表示
	}
}
