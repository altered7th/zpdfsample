package sample;

import java.io.IOException;

import com.cm55.pdfmonk.*;

import zpdf.ZPdf;
import zpdf.ZPdfCell;

/**
 * <pre>
 * 自動改ページでのヘッダー処理のサンプル
 * 
 * 自動改ページが行われると、ヘッダーは次ページ以降描画されない。
 * 自動改ページでも、改ページ後もヘッダーを描画するサンプル
 * </pre>
 */
public class Table1Sample extends ZPdf {

	public static void main( String[] args ) {
		new Table1Sample();
	}
	public Table1Sample() {
		try {
			createPdf();
		} catch ( IOException e ) {
			e.printStackTrace();
		}
	}

	private MkContext ctx;
	private MkTable table;
	private MkCanvas canvas;
	private ZPdfCell cell;
	
	private void createPdf() throws IOException {
		// 改ページ時のヘッダー描画分のスペースをマージンで確保する。
		MkGeometry A4_LANDSCAPE_HEADER = newGeoSize( 297, 210, 15, 5, 5, 5 );
		initPdf( "c.pdf", A4_LANDSCAPE_HEADER );
		drawTable();
		closePdf();
		browse();
	}

	private void drawTable() {
		ctx = getContext();
		cell = new ZPdfCell( ctx );
		canvas = getCanvas();
		MkDocument doc = getDoc();

		// 改ページコールバック
		doc.setNewPageCallback( callback-> {
			int page = (int)callback;
			System.out.println( "New Page:" + page );
//			drawFullOutline();
//			showGrid();
			// 1ページ目と改ページのY位置は微妙に異なる
			if ( page == 1 ) {
				pageText( page, -13 );
			} else if ( page > 1 ) {
				overHeader();
				pageText( page, -12 );
			}
		});
		
		firstHeader();
		body();
		table.addToCanvas( canvas );
	}
	private void pageText( int page, float y ) {
		text( "page : " + page, 0, y, 3.5F );
	}
	private void body() {
		for ( int i=0;i<61;i++ ) {
			table.addCell( cell.middle( i + ".労務管理の状態" ) );
			table.addCell( cell.right( i + "/300" ) );
			table.addCell( cell.middle( "" ) );
			table.addCell( cell.middle( "" ) );
		}
	}
	private void firstHeader() {
		table = newTable();
		canvas.setVertical( MkUnit.MM, -8 );	// ヘッダーのY位置
	}
	private void overHeader() {
		MkTable table = newTable();
		canvas.setVertical( MkUnit.MM, -7 );
		table.addToCanvas( canvas );
	}
	private MkTable newTable() {
		MkTable table = new MkTable( ctx, 4 );
		table.setColumnRatios( canvas.getGeometry().getPrintWidth(),
			new float[] { 50, 30, 30, 100 } );
		for (String name : new String[] { "分類／項目名", "配点", "判定", "項目内容" }) {
			table.addCell( cell.center( name, "d3d3d3" ) );
		}
		return table;
	}
}
