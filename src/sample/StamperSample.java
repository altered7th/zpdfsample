package sample;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.io.File;
import java.io.IOException;

import com.cm55.pdfmonk.MkBaseFont;
import com.cm55.pdfmonk.MkCanvas;
import com.cm55.pdfmonk.MkContentByte;
import com.cm55.pdfmonk.MkContext;
import com.cm55.pdfmonk.MkDocument;
import com.cm55.pdfmonk.MkEncoding;
import com.cm55.pdfmonk.MkFont;
import com.cm55.pdfmonk.MkFontFace;
import com.cm55.pdfmonk.MkGeometry;
import com.cm55.pdfmonk.MkStamper;
import com.cm55.pdfmonk.MkTextBlock;
import com.cm55.pdfmonk.MkUnit;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.BaseFont;

import zpdf.ZPdf;

public class StamperSample extends ZPdf {

	public static void main( String[] args ) {
		StamperSample x = new StamperSample();
		try {
			x.exec();
		} catch ( IOException e ) {
			e.printStackTrace();
		}
	}

	public void exec() throws IOException {
	
		// 元PDF hello.pdf を使用して、hello2.pdf を作成する。
		MkStamper stamper = initStamper( "hello.pdf", "hello2.pdf", ZPdf.A4_LANDSCAPE );
		
		//-----------
		// page 1
		selectPage( 1 );

		text( "総ページ数：" + stamper.pageCount(), 10, 30, 10 );
		text( "うううううううううABCabc", 100, 150, 10 );
		text( "BBBB", 100, 160, 5 );
		
		int y = 0;
		for ( int i=0;i<10;i++ ) {
			line( 0, 30 + y, 120, 30 + y, Color.red, 0.2F );
			y += 10;
		}

		//-----------
		// page 2
		selectPage( 2 );
		y = 0;
		for ( int i=0;i<10;i++ ) {
			line( 40, 30 + y, 160, 30 + y, Color.blue, 0.2F );
			y += 10;
		}
		
		stamper.close();	// closeStamper();
		browse();
	}
}
