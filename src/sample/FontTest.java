package sample;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;

// フォントファイルを指定する場合
public class FontTest {
	public static void main(String[] args) throws Exception {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		Document document = new Document();
		PdfWriter.getInstance(document, bos);
		document.open();
		BaseFont bf = BaseFont.createFont("sazanami-gothic.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
		Font font = new Font(bf, 25);
		document.add(new Paragraph("Windowsでコンピューターの世界が広がります", font));
		document.add(new Paragraph("あいうえお", font));
//		document.add(new Paragraph("Windowsでコンピューターの世界が広がります" ));
		document.close();

		FileOutputStream fos = new FileOutputStream(new File("output.pdf"));
		fos.write(bos.toByteArray());
		fos.close();
	}
	// TODO: iText 全てのフォントを指定する
	// http://www.orquesta.org/takegata/Article/ArticleView.jsp?article_id=799
}
