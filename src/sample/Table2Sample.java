package sample;

import java.io.IOException;

import com.cm55.pdfmonk.*;

import zpdf.ZPdf;
import zpdf.ZPdfCell;

/**
 * <pre>
 * 自分で改ページを行うサンプル
 * 描画する行数によって自分で改ページ処理を行う。
 * </pre>
 */
public class Table2Sample extends ZPdf {

	public static void main( String[] args ) {
		new Table2Sample();
	}
	public Table2Sample() {
		try {
			createPdf();
		} catch ( IOException e ) {
			e.printStackTrace();
		}
	}

	private MkContext ctx;
	private MkTable table;
	private MkCanvas canvas;
	private ZPdfCell cell;
	
	private void createPdf() throws IOException {
		initPdf( "c.pdf", A4_LANDSCAPE );
		drawTable();
		closePdf();
		browse();
	}

	private void drawTable() {
		ctx = getContext();
		cell = new ZPdfCell( ctx );
		canvas = getCanvas();
		MkDocument doc = getDoc();

		doc.setNewPageCallback( callback-> {
			int page = (int)callback;
			System.out.println( "New Page:" + page );
//			drawFullOutline();
//			showGrid();
			pageText( page );
		});
		
		createHeader();
		createBody();
		table.addToCanvas( canvas );
	}
	private void pageText( int page ) {
		text( "page : " + page, 0, 0, 3.5F );
	}
	private void createBody() {
		for ( int i=0;i<81;i++ ) {
			if ( i > 0 && i % 25 == 0 ) {
				newPage();
			}
			table.addCell( cell.middle( i + ".労務管理の状態" ) );
			table.addCell( cell.right( i + "/300" ) );
			table.addCell( cell.middle( "" ) );
			table.addCell( cell.middle( "" ) );
		}
	}
	private void createHeader() {
		table = newTable();
		canvas.setVertical( MkUnit.MM, 10 );
	}
	private MkTable newTable() {
		MkTable table = new MkTable( ctx, 4 );
		table.setColumnRatios( canvas.getGeometry().getPrintWidth(),
			new float[] { 50, 30, 30, 100 } );
		for (String name : new String[] { "分類／項目名", "配点", "判定", "項目内容" }) {
			table.addCell( cell.center( name, "d3d3d3" ) );
		}
		return table;
	}
	private void newPage() {
		table.addToCanvas( canvas );
		canvas.newPage();
		createHeader();
	}
}
