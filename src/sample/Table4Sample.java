package sample;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cm55.pdfmonk.*;
import com.itextpdf.text.Image;

import zpdf.ZPdf;
import zpdf.ZPdfCell;

/**
 * 画像描画のテーブルサンプル
 * Rowa で実際に運用しているものをサンプル化した
 */
public class Table4Sample extends ZPdf {
	public static void main( String[] args ) {
		new Table4Sample();
	}
	public Table4Sample() {
		try {
			createPdf();
		} catch ( IOException e ) {
			e.printStackTrace();
		}
	}
	private MkContext ctx;
	private MkTable table;
	private MkCanvas canvas;
	private ZPdfCell cell;
	private MkDocument doc;
	private MkPageNumbering pageNumbering;

	private void createPdf() throws IOException {
		List<MockDto> dtoList = createMockData();
		init();
		String acceptDate = "2020-07-01";
		printTitle( acceptDate, dtoList.size() );
		printList( dtoList );
		drawPageNumber( ctx, pageNumbering, doc.getPageNumber() );

		closePdf();
		browse();
	}
	private void printTitle( String acceptDate, int size ) {
		ctx.setFont( MkUnit.MM, 6 );
		new MkTextBlock( ctx, "空き箱一覧").setLimitWidth( getGeo().getPrintWidth() )
			.setBlockAlign(MkAlign.CENTER)
			.setToContentByte( canvas, MkUnit.MM, 0, -10-5 )
			;
		ctx.setFont( MkUnit.MM, 3 );
		
		new MkTextBlock( ctx, size + "件 受付日：" + acceptDate.replaceAll( "-", "/" ) ).setLimitWidth( getGeo().getPrintWidth() )
			.setBlockAlign( MkAlign.RIGHT )
			.setToContentByte(canvas, MkUnit.MM, 0, -5 )
			;
	}
	private void createTable() {
		// 1ページ目のヘッダーは table 本体の一部とする。（改ページ時のヘッダーとは異なる）
		table = createTableBase();
	}
	private void printNewPageHeader() {
		// 改ページ時のヘッダーは table 改ページ時に独立して処理する。
		canvas.setVertical( MkUnit.MM, -7f );
		MkTable headerTable = createTableBase();
		headerTable.addToCanvas( canvas );
	}
	private MkTable createTableBase() {
		MkTable t = new MkTable( ctx, 7 );
		t.setColumnRatios(canvas.getGeometry().getPrintWidth(), new float[] { 100, 80, 25, 60, 30, 30, 50 } );
		for (String name : new String[] { "製品名", "包装規格", "包装数", "メーカー", "空き箱数", "部品数量", "商品コード" }) {
			t.addCell( cell.middle( name ).setBgColor( "F0F8FF" ) );
		}
		return t;
	}
	private void printList( List<MockDto> dtoList ) throws IOException {
		for ( MockDto dto : dtoList ) {
			printRow( dto, cell );
		}
		table.addToCanvas( canvas );
	}
	private void printRow( MockDto dto, ZPdfCell cell ) throws IOException {
		table.addCell( cell.middle( dto.getString( "drug_name" ) ) );
		table.addCell( cell.middle( dto.getString( "package_standard" ) ) );
		table.addCell( cell.right( dto.getString( "package_quantity" ) ) );
		table.addCell( cell.middle( dto.getString( "manufacturer_name" ) ) );
		table.addCell( cell.right( String.valueOf( dto.getInteger( "pack_count" ) ) ) );
		table.addCell( cell.right( String.valueOf( dto.getDouble( "item_quantity" ) ) ) );
		table.getITextTable().addCell( createBarcodeImage( dto.getString( "product_code" ) ) );
	}
	private Image createBarcodeImage( String productCode ) throws IOException {
//		String fileName = "images/barcode/" + productCode + ".png";
//		MkImage mkImage = new MkImage( new File( fileName ) );
		File file = new File( DrawSample.class.getResource( "barcode.png" ).getFile() );
		MkImage mkImage = new MkImage( file );
		Image image = mkImage.getITextImage();
		return image;
	}
	private void init() throws IOException {
		initPdf( "c.pdf", A4_PORTRAIT );
//		drawFullOutline();
//		showGrid();	
		
		canvas = getCanvas();
		doc = getDoc();
		ctx = canvas.getContext();
		cell = new ZPdfCell( ctx );
		createTable();

		MkPageNumbering pageNumbering = setPageNumbering( canvas );
		NewPageCallback callback = new NewPageCallback( pageNumbering );
		doc.setNewPageCallback( callback::create );
	}
	void drawPageNumber(MkContext ctx, MkPageNumbering pageNumbering, int totalPages) {
		pageNumbering.pageSlots().forEach(slot -> {
			new MkTextBlock(ctx, "Page : " + slot.pageNumber + " / " + totalPages )
					.setToContentByte( slot.template, MkDimension.ZERO );
		});
	}
	class NewPageCallback {
		private MkPageNumbering pageNumbering;
		public NewPageCallback( MkPageNumbering pageNumbering ) {
			this.pageNumbering = pageNumbering;
		}
		public void create(int pageNumber) {
//			showGrid();
			pageNumbering.create( pageNumber );
			if ( pageNumber > 1 ) {
				printNewPageHeader();
			}
		}
	}
	private MkPageNumbering setPageNumbering( MkCanvas canvas ) {
		MkGeometry geo = canvas.getGeometry();
		pageNumbering = new MkPageNumbering( canvas, MkUnit.MM, geo.getPrintWidth().mmValue()-15, -10.0f-5, 20.0f, 10.0f );
		return pageNumbering;
	}
	private List<MockDto> createMockData() {
		List<MockDto> dtoList = new ArrayList<MockDto>();
		for ( int i=0;i<100;i++ ) {
			MockDto dto = new MockDto();
			dto.set( "drug_name", "製品名" + i );
			dto.set( "package_standard", "包装規格" + i );
			dto.set( "package_quantity", String.valueOf( i ) );
			dto.set( "manufacturer_name", "メーカー" + i );
			dto.set( "pack_count", i );
			dto.set( "item_quantity", (double)i );
			dto.set( "product_code", "barcode" );
			dtoList.add( dto );
		}
		return dtoList;
	}

	class MockDto {
		private Map<String,Object> dtoMap = null;
		public MockDto() {
			dtoMap = new HashMap<String,Object>();
		}
		public void set( String name, Object value ) {
			dtoMap.put( name, value );
		}
		public String getString( String name ) {
			Object value = dtoMap.get( name );
			return (String)value;
		}
		public Integer getInteger( String name ) {
			Object value = dtoMap.get( name );
			if ( value.getClass().getSimpleName().equals( "BigDecimal" ) ) {
				return ((BigDecimal)value).intValue();
			} else {
				return (Integer)value;
			}
		}
		public Double getDouble( String name ) {
			Object value = dtoMap.get( name );
			return (Double)value;
		}
	}
}
