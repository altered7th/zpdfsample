package sample;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;

import com.cm55.pdfmonk.*;

import zpdf.ZPdf;
import zpdf.ZPdfPara;

/**
 * 各種オブジェクトを描画する
 */
public class DrawSample extends ZPdf {
	public static void main( String[] args ) {
		new DrawSample();
	}
	public DrawSample() {
		try {
			createPdf();
		} catch ( IOException e ) {
			e.printStackTrace();
		}
	}
	private void createPdf() throws IOException {
		// c.pdf を A4横で作成する
		initPdf( "c.pdf", ZPdf.A4_LANDSCAPE );

		drawFullOutline();	// アウトラインを出力する
		showGrid();

//		drawText();
//		drawCenterText();
//		drawTextBlock();
//		drawLine();
//		drawBox();
		drawImage();
//		changeFont();

		closePdf();	// ドキュメントをクローズ
		browse();	// デスクトップに表示
	}
	private void drawBox() {
		box( 10, 0, 100, 10, Color.BLUE, 0.1F );
		
		BasicStroke stroke = new BasicStroke( 0.1F, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 1.0f, new float[] {3, 1}, 0);
		box( 10, 20, 100, 30, Color.BLUE, stroke );

		box( 10, 60, 100, 10, Color.BLUE, 1.0F );
	}
	private void drawLine() {
		float y = 0.0F;
		// 点線
		BasicStroke stroke = new BasicStroke( 0.1F, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 1.0f, new float[] {3, 1}, 0);
		for ( int i=0;i<10;i++ ) {
			line( 10, y, 100, y, Color.BLUE, 0.2F );
			line( 50, y+5, 150, y+5, Color.RED, stroke );
			y += 10.0;
		}
	}
	// テキストは改行できる
	// ボックスを描画する（線の太さは 0.1F に固定）
	void drawTextBlock() {
		String text = "〒 123-4567\n"
				+ "東京都千代田区九段下1-20-8\n"
				+ "千代田ビジネスビル7F\n"
				+ "\n" + "鈴木一郎　様";
		ZPdfPara p = new ZPdfPara();
		p.setText( text ).setX( 30 ).setY( 0 ).setWidth( 80 ).setHeight( 30 ).setBoxX( -2 ).setBoxY( -2 );
//		p.setText( text ).setX( 30 ).setY( 0 ).setWidth( 80 ).setHeight( 30 );
//		p.setText( text );
		textBlock( p );

		p.clear().setText( text ).setX( 30 ).setY( 70 ).setWidth( 80 ).setHeight( 30 )
			.setBoxX( -2 ).setBoxY( -2 ).setFontSize( 3.5f )
			.setTextColor( new MkColor( "00aa00" ) )
			;
		textBlock( p );

//		textBlock( text, 200, 0, 80, 30, -2, -2 );
	}
	// ブロック幅を指定してセンタリングする。
	void drawCenterText() {
		// 幅を指定しなければページ幅に合わせてセンタリングする
		drawCenter( "センター(x=0,y=0,size=5)", 0, 0, 5 );
		// メソッドチェーンを使った方法
		ZPdfPara p = new ZPdfPara();
		center( p.setText( "センター(x=0,y=10,size=5)" ).setX( 0 ).setY( 10 ).setFontSize( 5 ) );
//		center( p );

		center( "センター幅指定(x=0,y=20,font=5,w=200)\nxxxxxx", 0, 20, 5, 200 );
		box( 0, 20, 200, 15 );
	}
	private void drawText() {
		text( "サイズ6 x=10,y=0", 10, 0, 6 );
		text( "デフォルトサイズ（サイズ3) x=10,y=10", 10, 10 );
		text( "サイズ２.5 x=10,y=20", 10, 20, 2.5F );

		getContext().setFont( MkUnit.MM, 6 );
		text( "デフォルトサイズ（サイズ6に変更)", 10, 30 );

		ZPdfPara p = new ZPdfPara();
		p.setText( "〒154-8504 東京都世田谷区世田谷4丁目21番27号 · 03-5432-1111" ).setX( 10 ).setY( 50 )
			.setWidth( 80 ).setHeight( 30 )
			.setTextColor( new MkColor( "00aa00" ) )
			;
		textBlock( p );
	}
	private void changeFont() {
		drawText( "abcdefghijklmnopqustuvwxyzあいうえお", 0, 20, 6 );
		drawText( "abcdefghijklmnopqustuvwxyzあいうえお", 0, 27, 6, MkFontFace.HEISEI_KAKU_GOTHIC, MkEncoding.UniJIS_UCS2_H );
		drawText( "abcdefghijklmnopqustuvwxyzあいうえお", 0, 34, 6 );
	}
	private void drawImage() {
//		image( DrawSample.class.getResource( "dog.jpg" ), 30, 30, 50, 80, 80 );
		image( DrawSample.class.getResource( "dog.jpg" ), 30, 30, 50, 80, 80, true );
		
		File file1 = new File( DrawSample.class.getResource( "dog.jpg" ).getFile() );
		image( file1, 30, 100, 50 );
		
		image( DrawSample.class.getResource( "xxx.jpg" ), 100, 10, 100 );
		File file2 = new File( DrawSample.class.getResource( "xxx.jpg" ).getFile() );
		image( file2, 100, 80, 100 );
	}
}
