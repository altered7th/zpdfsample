package sample;

import java.io.IOException;

import com.cm55.pdfmonk.MkCanvas;
import com.cm55.pdfmonk.MkContext;
import com.cm55.pdfmonk.MkDimension;
import com.cm55.pdfmonk.MkDocument;
import com.cm55.pdfmonk.MkGeometry;
import com.cm55.pdfmonk.MkLen;
import com.cm55.pdfmonk.MkPageNumbering;
import com.cm55.pdfmonk.MkTable;
import com.cm55.pdfmonk.MkTextBlock;
import com.cm55.pdfmonk.MkUnit;

import zpdf.ZPdf;
import zpdf.ZPdfCell;

/**
 * テンプレート、 RowSpan を使ったテーブルサンプル 
 */
public class Table3Sample extends ZPdf {
	public static void main( String[] args ) {
		new Table3Sample();
	}

	private MkContext ctx;
	private MkCanvas canvas;
	private MkDocument doc;
	
	public Table3Sample() {
		try {
			createPdf();
		} catch ( IOException e ) {
			e.printStackTrace();
		}	
	}
	private void createPdf() throws IOException {
		initPdf( "c.pdf", A4_LANDSCAPE );
		ctx = getContext();
		canvas = getCanvas();
		doc = getDoc();
		
		MkPageNumbering pageNumbering = setPageNumbering( canvas );
		doc.setNewPageCallback( pageNumbering::create );
		
		table1();

		canvas.setVertical( MkUnit.MM, 35 );
		table2();
		
		canvas.advance( MkUnit.MM, 5 );
		table2();

		canvas.advance( MkUnit.MM, 5 );
		table2();

		canvas.advance( MkUnit.MM, 5 );
		table2();
		
		drawPageNumber( ctx, pageNumbering, doc.getPageNumber() );
		closePdf();
		browse();
	}
	private void table1() {
		ZPdfCell cell = new ZPdfCell( ctx );

		MkTable table = new MkTable( ctx, 4 );
		table.setSpacingBefore( new MkLen( MkUnit.MM, 2 ) );

		table.setColumnRatios( canvas.getGeometry().getPrintWidth().div( new MkLen( MkUnit.MM, 2 ) ),
				new float[] { 50, 100, 50, 100 } );
		String bgColor = "e0ffff";
		// row:1
		table.addCell( cell.center( "測定表名", bgColor ) );
		table.addCell( cell.text( "2018年度_下期_QT_本部監査" ) );
		table.addCell( cell.center( "実施日", bgColor ) );
		table.addCell( cell.text( "2018/06/26" ) );

		// row:2
		table.addCell( cell.center( "店舗名", bgColor ) );
		table.addCell( cell.text( "ＱＴ細川インター(998789)" ) );
		table.addCell( cell.center( "マネジャー名", bgColor ) );
		table.addCell( cell.text( "社員１２３４１２３４２８" ) );

		// row:3
		table.addCell( cell.center( "実施日時", bgColor ) );
		table.addCell( cell.text( "2018/06/26 19:50〜20:44" ) );
		table.addCell( cell.center( "実施者", bgColor ) );
		table.addCell( cell.text( "" ) );

		table.addToCanvas( canvas );
	}
	private void table2() {
		ZPdfCell cell = new ZPdfCell( ctx );

		MkTable table = new MkTable( ctx, 6 );
		table.setColumnRatios( canvas.getGeometry().getPrintWidth(),
			new float[] { 100, 100, 50, 50, 30, 40 } );
		for (String name : new String[] { "集計名", "分類名", "評価小計\n評価／配点", "評価得点\n評点／配点", "評語", "チェック" }) {
			table.addCell( cell.center( name, "e0ffff" ) );
		}
	
		int rowSpan;
		// row:1
		rowSpan = 2;
		table.addCell( cell.middle( "労務管理", rowSpan ) );
		table.addCell( cell.middle( "労務管理の状態" ) ); // col:1
		table.addCell( cell.right( "225/300" ) ); // col:2
		table.addCell( cell.right( "325/400", rowSpan ) );
		table.addCell( cell.center( "B", rowSpan ) );
		table.addCell( cell.right( "35/35", rowSpan ) );
		//
		table.addCell( cell.text( "著しい労務不良" ) );	// col:1
		table.addCell( cell.right( "100/100" ) );		// col:2
		
		// row:1
		table.addCell( cell.middle( "情報管理状態" ) );
		table.addCell( cell.middle( "情報管理の状態" ) );
		table.addCell( cell.right( "50/50" ) );
		table.addCell( cell.right( "50/50" ) );
		table.addCell( cell.center( "A" ) );
		table.addCell( cell.right( "50/50" ) );

		// row:3
		rowSpan = 4;
		table.addCell( cell.middle( "事務金銭管", rowSpan ) );
		table.addCell( cell.middle( "帳票管理の状態" ) ); // col:1
		table.addCell( cell.right( "28/30" ) ); 		// col:2
		table.addCell( cell.right( "141/150", rowSpan ) );
		table.addCell( cell.center( "A", rowSpan ) );
		table.addCell( cell.right( "29/29", rowSpan ) );
		// col:1,2
		table.addCell( cell.text( "現金管理の状態" ) );
		table.addCell( cell.right(  "37/42" ) );

		table.addCell( cell.text( "会計処理の状態" ) );
		table.addCell( cell.right( "6/8" ) );

		table.addCell( cell.text( "著しい事務金銭不良" ) );
		table.addCell( cell.right( "70/70" ) );
		
		// 総合計
		table.addCell( cell.blank() );
		table.addCell( cell.blank() );
		table.addCell( cell.center( "総合計" ) );
		table.addCell( cell.right( "806/1000" ) );
		table.addCell( cell.center( "B" ) );
		table.addCell( cell.right( "122/122" ) );

		table.addToCanvas( canvas );
	}
	//--------------------------------------------------------------------------
		void drawPageNumber(MkContext ctx, MkPageNumbering pageNumbering, int totalPages) {
			pageNumbering.pageSlots().forEach(slot -> {
				new MkTextBlock( ctx, "Page : " + slot.pageNumber + " / " + totalPages )
						.setToContentByte( slot.template, MkDimension.ZERO );
			});
		}
		private MkPageNumbering setPageNumbering( MkCanvas canvas ) {
			MkGeometry geo = canvas.getGeometry();
			MkPageNumbering pageNumbering = new MkPageNumbering( canvas, MkUnit.MM, 0f, geo.getPrintHeight().mmValue()-3, 20.0f, 10.0f );
			return pageNumbering;
		}
}
